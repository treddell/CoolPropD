#!/bin/bash

# CoolProp D Language Interface Installer

COOLPROP_INSTALL_DIR=$1

DIRECTORY=$(cd `dirname $0` && pwd)
COOLPROP_INLCUDE_DIR="$COOLPROP_INSTALL_DIR/include/"
COOLPROP_FMTLIB_DIR="$COOLPROP_INSTALL_DIR/externals/fmtlib/"
COOLPROP_STATIC_LIBRARY="$COOLPROP_INSTALL_DIR/build/libCoolProp.a"
SOURCE_DIR="$DIRECTORY/source"
CPP_LAYER="$SOURCE_DIR/cpp_layer.cpp"

# place symbolic link to static library in source directory
ln -sf $COOLPROP_STATIC_LIBRARY "$DIRECTORY/source/libCoolProp.a"

# compiler c++ interface functions
g++ -c -O2 "-L$SOURCE_DIR" "-I$SOURCE_DIR" "-I$COOLPROP_INLCUDE_DIR" $CPP_LAYER \
"-I$COOLPROP_FMTLIB_DIR" -o "$SOURCE_DIR/cpp_layer.o" -lCoolProp -ldl
