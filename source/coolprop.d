/* D side CoolProp Interface,

Thin wrappers over the cpp_layer.cpp layer with unittests
*/

import std.math: approxEqual, isNaN;
import std.string: toStringz, fromStringz, replace, startsWith;
import std.conv;
import std.exception: assertThrown;
import core.stdc.config: c_ulong;

extern(C++) 
{
    /// Necessary evil in order to catch and rethrow C++ exceptions
    // stores a double or an error string if thrown
    private class DoubleContainer
    {
        @disable this();

        final char* get_error_msg();
        final double get_value();
    }

    private void delete_double_container(ref DoubleContainer inst);

    private class VectorDoubleContainer
    {
        @disable this();

        final char* get_error_msg();
        final double* get_data();
        final ulong get_length();
    }

    private void delete_vector_double_container(ref VectorDoubleContainer inst);

    // Wrap the high level interfaces
    private DoubleContainer _Props1SI(const char* fluidName, const char* output);

    private DoubleContainer _PropsSI(const char* output,
                                     const char* name1, 
                                     double prop1,
                                     const char* name2,
                                     double prop2,
                                     const char* fluidName);

    private DoubleContainer _saturation_ancillary(const char* fluidName, 
                                                  const char* output,
                                                  int Q,
                                                  const char* input,
                                                  double value);

    private char* _PhaseSI(const char* name1,
                           double prop1,
                           const char* name2,
                           double prop2,
                           const char* fluidName);

    private int _generate_update_pair(int key1,
                                      double value1,
                                      int key2,
                                      double value2,
                                      ref double out1,
                                      ref double out2);

    private class _AbstractState
    {
        @disable this();
        final const(char*) update(int input_pair, double value1, double value2);
        final const(char*) get_error_msg() const;
        final const(char*) name() const;
        final double T() const;
        final double rhomolar() const;
        final double rhomass() const;
        final double p() const;
        final double Q() const;
        final double tau() const;
        final double delta() const;
        final double molar_mass() const;
        final double acentric_factor() const;
        final double gas_constant() const;
        final double Bvirial() const;
        final double dBvirial_dT() const;
        final double Cvirial() const;
        final double dCvirial_dT() const;
        final double compressibility_factor() const;
        final double hmolar() const;
        final double hmass() const;
        final double hmolar_excess() const;
        final double hmass_excess() const;
        final double smolar() const;
        final double smass() const;
        final double smolar_excess() const;
        final double smass_excess() const;
        final double umolar() const;
        final double umass() const;
        final double umolar_excess() const;
        final double umass_excess() const;
        final double cpmolar() const;
        final double cpmass() const;
        final double cp0molar() const;
        final double cp0mass() const;
        final double cvmolar() const;
        final double cvmass() const;
        final double gibbsmolar() const;
        final double gibbsmass() const;
        final double gibbsmolar_excess() const;
        final double gibbsmass_excess() const;
        final double helmholtzmolar() const;
        final double helmholtzmass() const;
        final double helmholtzmolar_excess() const;
        final double helmholtzmass_excess() const;
        final double volumemolar_excess() const;
        final double volumemass_excess() const;
        final double speed_sound() const;
        final double isothermal_compressibility() const;
        final double isobaric_expansion_coefficient() const;
        final double fugacity_coefficient(c_ulong i) const;
        final double fugacity(c_ulong i) const;
        final double chemical_potential(c_ulong i) const;
        final const(char*) backend_name() const;
        final bool clear();
        final int phase() const;
        final const(char*) specify_phase(int phase);
        final const(char*) unspecify_phase();
        final double Tmin() const;
        final double Tmax() const;
        final double pmax() const;
        final double Ttriple() const;
        final double T_critical() const;
        final double p_critical() const;
        final double rhomolar_critical() const;
        final double rhomass_critical() const;
        final double T_reducing() const;
        final double rhomolar_reducing() const;
        final double rhomass_reducing() const;
        final double p_triple() const;

        final DoubleContainer dipole_moment()  const;
        final double viscosity() const;
        final double conductivity() const;
        final double Prandtl() const;
        final double keyed_output(int key) const;
        final double trivial_keyed_output(int key) const;
        final double saturated_liquid_keyed_output(int key) const;
        final double saturated_vapor_keyed_output(int key) const;

        final double fundamental_derivative_of_gas_dynamics() const;
        final double PIP() const;
        final DoubleContainer surface_tension() const;
        final DoubleContainer first_two_phase_deriv(int Of, int Wrt, int Constant) const;
        final DoubleContainer second_two_phase_deriv(int Of,
                                                     int Wrt1,
                                                     int Constant1,
                                                     int Wrt2,
                                                     int Constant2) const;
        final DoubleContainer first_two_phase_deriv_splined(int Of, int Wrt, int Constant, double x_end) const;
    
        final DoubleContainer first_partial_deriv(int Of, int Wrt, int Constant) const;
        final DoubleContainer second_partial_deriv(int Of1,
                                                   int Wrt1,
                                                   int Constant1,
                                                   int Wrt2,
                                                   int Constant2) const;
        final DoubleContainer first_saturation_deriv(int Of1, int Wrt1) const;
        final DoubleContainer second_saturation_deriv(int Of1, int Wrt1, int Wrt2) const;
        final double alpha0() const;
        final double dalpha0_dDelta() const;
        final double dalpha0_dTau() const;
        final double d2alpha0_dDelta2() const;
        final double d2alpha0_dDelta_dTau() const;
        final double d2alpha0_dTau2() const;
        final double d3alpha0_dTau3() const;
        final double d3alpha0_dDelta_dTau2() const;
        final double d3alpha0_dDelta2_dTau() const;
        final double d3alpha0_dDelta3() const;
        final double alphar() const;
        final double dalphar_dDelta() const;
        final double dalphar_dTau() const;
        final double d2alphar_dDelta2() const;
        final double d2alphar_dDelta_dTau() const;
        final double d2alphar_dTau2() const;
        final double d3alphar_dDelta3() const;
        final double d3alphar_dDelta2_dTau() const;
        final double d3alphar_dDelta_dTau2() const;
        final double d3alphar_dTau3() const;
        final double d4alphar_dDelta4() const;
        final double d4alphar_dDelta3_dTau() const;
        final double d4alphar_dDelta2_dTau2() const;
        final double d4alphar_dDelta_dTau3() const;
        final double d4alphar_dTau4() const;

        final const(char*) change_EOS(ulong i, const char* EOS_name);
        final const(char*) set_mole_fractions(double* mole_fractions, c_ulong size);
        final const(char*) set_mass_fractions(double* mass_fractions, c_ulong size);
        final const(char*) set_volu_fractions(double* volu_fractions, c_ulong size);

        final const(char*) set_reference_stateS(const char* reference_state);
        final const(char*) set_reference_stateD(double T, double rhomolar, double hmolar0, double smolar0);
        final const(char*) true_critical_point(double T, double rho) const;

        final VectorDoubleContainer mole_fractions_liquid() const;
        final VectorDoubleContainer mole_fractions_vapor() const;

        final const(char*) ideal_curve(const char* type,
                                       const double* T,
                                       ulong T_length,
                                       const double* p,
                                       ulong p_length) const;
        
        final const(char*) viscosity_contributions(double dilute,
                                                   double initial_density,
                                                   double residual,
                                                   double critical) const;
        
        final const(char*) conductivity_contributions(double dilute,
                                                      double initial_density,
                                                      double residual,
                                                      double critical) const;
        
        final const(char*) conformal_state(const char* reference_fluid,
                                           double T,
                                           double rhomolar) const;
    }

    private _AbstractState create_instance(const char* backend,
                                           const char* fluidName);
    
    private void delete_instance(ref _AbstractState inst);
}

/// Catch em all CoolProp Exception
class CoolPropException : Exception
{
    this(string msg, string file = __FILE__, size_t line = __LINE__)
    {
        super(msg, file, line);
    }
}

/// Input pairs
enum input_pairs
{
    INPUT_PAIR_INVALID = 0, // Default (invalid) value
    QT_INPUTS, ///< Molar quality, Temperature in K
    PQ_INPUTS, ///< Pressure in Pa, Molar quality
    QSmolar_INPUTS, ///< Molar quality, Entropy in J/mol/K
    QSmass_INPUTS, ///< Molar quality, Entropy in J/kg/K
    HmolarQ_INPUTS, ///< Enthalpy in J/mol, Molar quality
    HmassQ_INPUTS, ///< Enthalpy in J/kg, Molar quality
    DmolarQ_INPUTS, ///< Density in mol/m^3, Molar quality
    DmassQ_INPUTS,  ///< Density in kg/m^3, Molar quality
    PT_INPUTS, ///< Pressure in Pa, Temperature in K
    DmassT_INPUTS, ///< Mass density in kg/m^3, Temperature in K
    DmolarT_INPUTS, ///< Molar density in mol/m^3, Temperature in K
    HmolarT_INPUTS, ///< Enthalpy in J/mol, Temperature in K
    HmassT_INPUTS, ///< Enthalpy in J/kg, Temperature in K
    SmolarT_INPUTS, ///< Entropy in J/mol/K, Temperature in K
    SmassT_INPUTS, ///< Entropy in J/kg/K, Temperature in K
    TUmolar_INPUTS, ///< Temperature in K, Internal energy in J/mol
    TUmass_INPUTS, ///< Temperature in K, Internal energy in J/kg
    DmassP_INPUTS, ///< Mass density in kg/m^3, Pressure in Pa
    DmolarP_INPUTS, ///< Molar density in mol/m^3, Pressure in Pa
    HmassP_INPUTS, ///< Enthalpy in J/kg, Pressure in Pa
    HmolarP_INPUTS, ///< Enthalpy in J/mol, Pressure in Pa
    PSmass_INPUTS, ///< Pressure in Pa, Entropy in J/kg/K
    PSmolar_INPUTS, ///< Pressure in Pa, Entropy in J/mol/K
    PUmass_INPUTS, ///< Pressure in Pa, Internal energy in J/kg
    PUmolar_INPUTS, ///< Pressure in Pa, Internal energy in J/mol
    HmassSmass_INPUTS, ///< Enthalpy in J/kg, Entropy in J/kg/K
    HmolarSmolar_INPUTS, ///< Enthalpy in J/mol, Entropy in J/mol/K
    SmassUmass_INPUTS, ///< Entropy in J/kg/K, Internal energy in J/kg
    SmolarUmolar_INPUTS, ///< Entropy in J/mol/K, Internal energy in J/mol
    DmassHmass_INPUTS, ///< Mass density in kg/m^3, Enthalpy in J/kg
    DmolarHmolar_INPUTS, ///< Molar density in mol/m^3, Enthalpy in J/mol
    DmassSmass_INPUTS, ///< Mass density in kg/m^3, Entropy in J/kg/K
    DmolarSmolar_INPUTS, ///< Molar density in mol/m^3, Entropy in J/mol/K
    DmassUmass_INPUTS, ///< Mass density in kg/m^3, Internal energy in J/kg
    DmolarUmolar_INPUTS, ///< Molar density in mol/m^3, Internal energy in J/mol
}

// Removes the input_pair namespace, mimicing the c++ interface
alias INPUT_PAIR_INVALID = input_pairs.INPUT_PAIR_INVALID;
alias QT_INPUTS = input_pairs.QT_INPUTS;
alias PQ_INPUTS = input_pairs.PQ_INPUTS;
alias QSmolar_INPUTS = input_pairs.QSmolar_INPUTS;
alias QSmass_INPUTS = input_pairs.QSmass_INPUTS;
alias HmolarQ_INPUTS = input_pairs.HmolarQ_INPUTS;
alias HmassQ_INPUTS = input_pairs.HmassQ_INPUTS;
alias DmolarQ_INPUTS = input_pairs.DmolarQ_INPUTS;
alias DmassQ_INPUTS = input_pairs.DmassQ_INPUTS;
alias PT_INPUTS = input_pairs.PT_INPUTS;
alias DmassT_INPUTS = input_pairs.DmassT_INPUTS;
alias DmolarT_INPUTS = input_pairs.DmolarT_INPUTS;
alias HmolarT_INPUTS = input_pairs.HmolarT_INPUTS;
alias HmassT_INPUTS = input_pairs.HmassT_INPUTS;
alias SmolarT_INPUTS = input_pairs.SmolarT_INPUTS;
alias SmassT_INPUTS = input_pairs.SmassT_INPUTS;
alias TUmolar_INPUTS = input_pairs.TUmolar_INPUTS;
alias TUmass_INPUTS = input_pairs.TUmass_INPUTS;
alias DmassP_INPUTS = input_pairs.DmassP_INPUTS;
alias DmolarP_INPUTS = input_pairs.DmolarP_INPUTS;
alias HmassP_INPUTS = input_pairs.HmassP_INPUTS;
alias HmolarP_INPUTS = input_pairs.HmolarP_INPUTS;
alias PSmass_INPUTS = input_pairs.PSmass_INPUTS;
alias PSmolar_INPUTS = input_pairs.PSmolar_INPUTS;
alias PUmass_INPUTS = input_pairs.PUmass_INPUTS;
alias PUmolar_INPUTS = input_pairs.PUmolar_INPUTS;
alias HmassSmass_INPUTS = input_pairs.HmassSmass_INPUTS;
alias HmolarSmolar_INPUTS = input_pairs.HmolarSmolar_INPUTS;
alias SmassUmass_INPUTS = input_pairs.SmassUmass_INPUTS;
alias SmolarUmolar_INPUTS = input_pairs.SmolarUmolar_INPUTS;
alias DmassHmass_INPUTS = input_pairs.DmassHmass_INPUTS;
alias DmolarHmolar_INPUTS = input_pairs.DmolarHmolar_INPUTS;
alias DmassSmass_INPUTS = input_pairs.DmassSmass_INPUTS;
alias DmolarSmolar_INPUTS = input_pairs.DmolarSmolar_INPUTS;
alias DmassUmass_INPUTS = input_pairs.DmassUmass_INPUTS;
alias DmolarUmolar_INPUTS = input_pairs.DmolarUmolar_INPUTS;

/// CoolProp phases
enum phases
{
    iphase_liquid, ///< Subcritical liquid 
    iphase_supercritical, ///< Supercritical (p > pc, T > Tc)
    iphase_supercritical_gas, ///< Supercritical gas (p < pc, T > Tc)
    iphase_supercritical_liquid, ///< Supercritical liquid (p > pc, T < Tc)
    iphase_critical_point, ///< At the critical point
    iphase_gas, ///< Subcritical gas
    iphase_twophase, ///< Twophase
    iphase_unknown, ///< Unknown phase
    iphase_not_imposed ///< Phase is not imposed
}

// Removes the input_pair namespace, mimicing the c++ interface
alias iphase_liquid = phases.iphase_liquid;
alias iphase_supercritical = phases.iphase_supercritical;
alias iphase_supercritical_gas = phases.iphase_supercritical_gas;
alias iphase_supercritical_liquid = phases.iphase_supercritical_liquid;
alias iphase_critical_point = phases.iphase_critical_point;
alias iphase_gas = phases.iphase_gas;
alias iphase_twophase = phases.iphase_twophase;
alias iphase_unknown = phases.iphase_unknown;
alias iphase_not_imposed = phases.iphase_not_imposed;

// CoolProp input parameters
enum parameters
{
    INVALID_PARAMETER = 0,
    
    // General parameters
    igas_constant, ///< Ideal-gas constant
    imolar_mass, ///< Molar mass
    iacentric_factor, ///< Acentric factor
    irhomolar_reducing, ///< Molar density used for the reducing state
    irhomolar_critical, ///< Molar density used for the critical point
    iT_reducing, ///< Temperature at the reducing state
    iT_critical, ///< Temperature at the critical point
    irhomass_reducing, ///< Mass density at the reducing state
    irhomass_critical, ///< Mass density at the critical point
    iP_critical, ///< Pressure at the critical point
    iP_reducing, ///< Pressure at the reducing point
    iT_triple, ///< Triple point temperature
    iP_triple, ///< Triple point pressure
    iT_min, ///< Minimum temperature
    iT_max, ///< Maximum temperature
    iP_max, ///< Maximum pressure
    iP_min, ///< Minimum pressure
    idipole_moment, ///< Dipole moment

    // Bulk properties
    iT,  ///< Temperature
    iP, ///< Pressure
    iQ, ///< Vapor quality
    iTau, ///< Reciprocal reduced temperature
    iDelta, ///< Reduced density

    // Molar specific thermodynamic properties
    iDmolar, ///< Mole-based density
    iHmolar, ///< Mole-based enthalpy
    iSmolar, ///< Mole-based entropy
    iCpmolar, ///< Mole-based constant-pressure specific heat
    iCp0molar, ///< Mole-based ideal-gas constant-pressure specific heat
    iCvmolar, ///< Mole-based constant-volume specific heat
    iUmolar, ///< Mole-based internal energy
    iGmolar, ///< Mole-based Gibbs energy
    iHelmholtzmolar, ///< Mole-based Helmholtz energy
    iSmolar_residual, ///< The residual molar entropy (s^r/R = tau*dar_dtau-ar)

    // Mass specific thermodynamic properties
    iDmass, ///< Mass-based density
    iHmass, ///< Mass-based enthalpy
    iSmass, ///< Mass-based entropy
    iCpmass, ///< Mass-based constant-pressure specific heat
    iCp0mass, ///< Mass-based ideal-gas specific heat
    iCvmass, ///< Mass-based constant-volume specific heat
    iUmass, ///< Mass-based internal energy
    iGmass, ///< Mass-based Gibbs energy
    iHelmholtzmass, ///< Mass-based Helmholtz energy

    // Transport properties
    iviscosity, ///< Viscosity
    iconductivity, ///< Thermal conductivity
    isurface_tension, ///< Surface tension
    iPrandtl, ///< The Prandtl number

    // Derivative-based terms
    ispeed_sound, ///< Speed of sound
    iisothermal_compressibility, ///< Isothermal compressibility
    iisobaric_expansion_coefficient, ///< Isobaric expansion coefficient

    // Fundamental derivative of gas dynamics
    ifundamental_derivative_of_gas_dynamics, ///< The fundamental derivative of gas dynamics

    // Derivatives of the residual non-dimensionalized Helmholtz energy with respect to the EOS variables
    ialphar, 
    idalphar_dtau_constdelta, 
    idalphar_ddelta_consttau,

    // Derivatives of the ideal-gas non-dimensionalized Helmholtz energy with respect to the EOS variables
    ialpha0, 
    idalpha0_dtau_constdelta, 
    idalpha0_ddelta_consttau,

    // Other functions and derivatives
    iBvirial, ///< Second virial coefficient
    iCvirial, ///< Third virial coefficient
    idBvirial_dT, ///< Derivative of second virial coefficient with temperature
    idCvirial_dT, ///< Derivative of third virial coefficient with temperature
    iZ, ///< The compressibility factor Z = p*v/(R*T)
    iPIP, ///< The phase identification parameter of Venkatarathnam and Oellrich
    
    // Accessors for incompressibles
    ifraction_min, ///< The minimum fraction (mole, mass, volume) for incompressibles
    ifraction_max, ///< The maximum fraction (mole,mass,volume) for incompressibles
    iT_freeze, ///< The freezing temperature for incompressibles

    // Environmental parameters
    iGWP20, ///< The 20-year global warming potential
    iGWP100, ///< The 100-year global warming potential
    iGWP500, ///< The 500-year global warming potential
    iFH, ///< Fire hazard index
    iHH, ///< Health hazard index
    iPH, ///< Physical hazard index
    iODP, ///< Ozone depletion potential (R-11 = 1.0)
    iPhase, ///< The phase index of the given state
    iundefined_parameter ///< The last parameter, so we can check that all parameters are described in DataStructures.cpp
}

alias INVALID_PARAMETER = parameters.INVALID_PARAMETER;
alias igas_constant = parameters.igas_constant;
alias imolar_mass = parameters.imolar_mass;
alias iacentric_factor = parameters.iacentric_factor;
alias irhomolar_reducing = parameters.irhomolar_reducing;
alias irhomolar_critical = parameters.irhomolar_critical;
alias iT_reducing = parameters.iT_reducing;
alias iT_critical = parameters.iT_critical;
alias irhomass_reducing = parameters.irhomass_reducing;
alias irhomass_critical = parameters.irhomass_critical;
alias iP_critical = parameters.iP_critical;
alias iP_reducing = parameters.iP_reducing;
alias iT_triple = parameters.iT_triple;
alias iP_triple = parameters.iP_triple;
alias iT_min = parameters.iT_min;
alias iT_max = parameters.iT_max;
alias iP_max = parameters.iP_max;
alias iP_min = parameters.iP_min;
alias idipole_moment = parameters.idipole_moment;
alias iT = parameters.iT;
alias iP = parameters.iP;
alias iQ = parameters.iQ;
alias iTau = parameters.iTau;
alias iDelta = parameters.iDelta;
alias iDmolar = parameters.iDmolar;
alias iHmolar = parameters.iHmolar;
alias iSmolar = parameters.iSmolar;
alias iCpmolar = parameters.iCpmolar;
alias iCp0molar = parameters.iCp0molar;
alias iCvmolar = parameters.iCvmolar;
alias iUmolar = parameters.iUmolar;
alias iGmolar = parameters.iGmolar;
alias iHelmholtzmolar = parameters.iHelmholtzmolar;
alias iSmolar_residual = parameters.iSmolar_residual;
alias iDmass = parameters.iDmass;
alias iHmass = parameters.iHmass;
alias iSmass = parameters.iSmass;
alias iCpmass = parameters.iCpmass;
alias iCp0mass = parameters.iCp0mass;
alias iCvmass = parameters.iCvmass;
alias iUmass = parameters.iUmass;
alias iGmass = parameters.iGmass;
alias iHelmholtzmass = parameters.iHelmholtzmass;
alias iviscosity = parameters.iviscosity;
alias iconductivity = parameters.iconductivity;
alias isurface_tension = parameters.isurface_tension;
alias iPrandtl = parameters.iPrandtl;
alias ispeed_sound = parameters.ispeed_sound;
alias iisothermal_compressibility = parameters.iisothermal_compressibility;
alias iisobaric_expansion_coefficient = parameters.iisobaric_expansion_coefficient;
alias ifundamental_derivative_of_gas_dynamics = parameters.ifundamental_derivative_of_gas_dynamics;
alias ialphar = parameters.ialphar; 
alias idalphar_dtau_constdelta = parameters.idalphar_dtau_constdelta; 
alias idalphar_ddelta_consttau = parameters.idalphar_ddelta_consttau;
alias ialpha0 = parameters.ialpha0; 
alias idalpha0_dtau_constdelta = parameters.idalpha0_dtau_constdelta; 
alias idalpha0_ddelta_consttau = parameters.idalpha0_ddelta_consttau;
alias iBvirial = parameters.iBvirial;
alias iCvirial = parameters.iCvirial;
alias idBvirial_dT = parameters.idBvirial_dT;
alias idCvirial_dT = parameters.idCvirial_dT;
alias iZ = parameters.iZ;
alias iPIP = parameters.iPIP;
alias ifraction_min = parameters.ifraction_min;
alias ifraction_max = parameters.ifraction_max;
alias iT_freeze = parameters.iT_freeze;
alias iGWP20 = parameters.iGWP20; 
alias iGWP100 = parameters.iGWP100; 
alias iGWP500 = parameters.iGWP500; 
alias iFH = parameters.iFH; 
alias iHH = parameters.iHH; 
alias iPH = parameters.iPH; 
alias iODP = parameters.iODP; 
alias iPhase = parameters.iPhase; 
alias iundefined_parameter = parameters.iundefined_parameter; 

/** The D AbstractState public class */
class AbstractState 
{
    private _AbstractState _state;

    this(string backend, string fluidName)
    {
        _state = create_instance(backend.toStringz, fluidName.toStringz);

        // If an exception occured on the c++ side, rethrow it
        auto msg = _state.get_error_msg.to!string;
        if(msg != "") throw new CoolPropException(msg);
    }

    ~this() {_state.delete_instance;}

    void update(input_pairs pair, double value1, double value2)
    /// Update the state based off an input_pair specification, and two values
    {
        auto exit_msg = _state.update(pair, value1, value2).to!string;

        if(exit_msg != "") throw new CoolPropException(exit_msg);
    }

    AbstractState dup()
    // create a copy of the state
    {
        auto copy = new AbstractState(this.backend_name, this.name);
        // Update state based off density and internal energy
        copy.update(DmassUmass_INPUTS, this.rhomass, this.umass);
        
        return copy;
    }

    @property
    {
        string name() const {return _state.name.to!string;}
        double T() const {return _state.T();}
        double rhomolar() const {return _state.rhomolar();}
        double rhomass() const {return _state.rhomass();}
        double p() const { return _state.p();}
        double Q() const { return _state.Q();}
        double tau() const { return _state.tau();}
        double delta() const { return _state.delta();}
        double molar_mass() const { return _state.molar_mass();}
        double acentric_factor() const { return _state.acentric_factor();}
        double gas_constant() const { return _state.gas_constant();}
        double Bvirial() const { return _state.Bvirial();}
        double Cvirial() const { return _state.Cvirial();}
        double compressibility_factor() const { return _state.compressibility_factor();}
        double hmolar() const { return _state.hmolar();}
        double hmass() const { return _state.hmass();}
        double hmolar_excess() const { return _state.hmolar_excess();}
        double hmass_excess() const { return _state.hmass_excess();}
        double smolar() const { return _state.smolar();}
        double smass() const { return _state.smass();}
        double smolar_excess() const { return _state.smolar_excess();}
        double smass_excess() const { return _state.smass_excess();}
        double umolar() const { return _state.umolar();}
        double umass() const { return _state.umass();}
        double umolar_excess() const { return _state.umolar_excess();}
        double umass_excess() const { return _state.umass_excess();}
        double cpmolar() const { return _state.cpmolar();}
        double cpmass() const { return _state.cpmass();}
        double cp0molar() const { return _state.cp0molar();}
        double cp0mass() const { return _state.cp0mass();}
        double cvmolar() const { return _state.cvmolar();}
        double cvmass() const { return _state.cvmass();}
        double gibbsmolar() const { return _state.gibbsmolar();}
        double gibbsmass() const { return _state.gibbsmass();}
        double gibbsmolar_excess() const { return _state.gibbsmolar_excess();}
        double gibbsmass_excess() const { return _state.gibbsmass_excess();}
        double helmholtzmolar() const { return _state.helmholtzmolar();}
        double helmholtzmass() const { return _state.helmholtzmass();}
        double helmholtzmolar_excess() const { return _state.helmholtzmolar_excess();}
        double helmholtzmass_excess() const { return _state.helmholtzmass_excess();}
        double volumemolar_excess() const { return _state.volumemolar_excess();}
        double volumemass_excess() const { return _state.volumemass_excess();}
        double speed_sound() const { return _state.speed_sound();}
        double isothermal_compressibility() const { return _state.isothermal_compressibility();}
        double isobaric_expansion_coefficient() const { return _state.isobaric_expansion_coefficient();}
        double Tmin() const {return _state.Tmin();}
        double Tmax() const {return _state.Tmax();}
        double pmax() const {return _state.pmax();}
        double Ttriple() const {return _state.Ttriple();}
        double T_critical() const {return _state.T_critical();}
        double p_critical() const {return _state.p_critical();}
        double rhomolar_critical() const {return _state.rhomolar_critical();}
        double rhomass_critical() const {return _state.rhomass_critical();}
        double T_reducing() const {return _state.T_reducing();}
        double rhomolar_reducing() const {return _state.rhomolar_reducing();}
        double rhomass_reducing() const {return _state.rhomass_reducing();}
        double p_triple() const {return _state.p_triple();}
        phases phase() const {return cast(phases)_state.phase();}
        double alpha0() const {return _state.alpha0();}
        double dalpha0_dDelta() const {return _state.dalpha0_dDelta();}
        double dalpha0_dTau() const {return _state.dalpha0_dTau();}
        double d2alpha0_dDelta2() const {return _state.d2alpha0_dDelta2();}
        double d2alpha0_dDelta_dTau() const {return _state.d2alpha0_dDelta_dTau();}
        double d2alpha0_dTau2() const {return _state.d2alpha0_dTau2();}
        double d3alpha0_dTau3() const {return _state.d3alpha0_dTau3();}
        double d3alpha0_dDelta_dTau2() const {return _state.d3alpha0_dDelta_dTau2();}
        double d3alpha0_dDelta2_dTau() const {return _state.d3alpha0_dDelta2_dTau();}
        double d3alpha0_dDelta3() const {return _state.d3alpha0_dDelta3();}
        double alphar() const {return _state.alphar();}
        double dalphar_dDelta() const {return _state.dalphar_dDelta();}
        double dalphar_dTau() const {return _state.dalphar_dTau();}
        double d2alphar_dDelta2() const {return _state.d2alphar_dDelta2();}
        double d2alphar_dDelta_dTau() const {return _state.d2alphar_dDelta_dTau();}
        double d2alphar_dTau2() const {return _state.d2alphar_dTau2();}
        double d3alphar_dDelta3() const {return _state.d3alphar_dDelta3();}
        double d3alphar_dDelta2_dTau() const {return _state.d3alphar_dDelta2_dTau();}
        double d3alphar_dDelta_dTau2() const {return _state.d3alphar_dDelta_dTau2();}
        double d3alphar_dTau3() const {return _state.d3alphar_dTau3();}
        double d4alphar_dDelta4() const {return _state.d4alphar_dDelta4();}
        double d4alphar_dDelta3_dTau() const {return _state.d4alphar_dDelta3_dTau();}
        double d4alphar_dDelta2_dTau2() const {return _state.d4alphar_dDelta2_dTau2();}
        double d4alphar_dDelta_dTau3() const {return _state.d4alphar_dDelta_dTau3();}
        double d4alphar_dTau4() const {return _state.d4alphar_dTau4();}
        double dBvirial_dT() const  { return _state.dBvirial_dT(); }
        double dCvirial_dT() const  { return _state.dCvirial_dT();}
        double viscosity() const  { return _state.viscosity();}
        double conductivity() const  { return _state.conductivity();}
        double Prandtl() const  { return _state.Prandtl();}
        double fundamental_derivative_of_gas_dynamics() const  { return _state.fundamental_derivative_of_gas_dynamics();}
        double PIP() const { return _state.PIP();}
        double surface_tension() const
        {
            auto container = _state.surface_tension();
            scope(exit) delete_double_container(container);

            auto msg = container.get_error_msg.to!string;
            if (msg != "") {throw new CoolPropException(msg);}
            auto result = container.get_value();

            return result;
        }

        double dipole_moment() const
        {
            auto container = _state.dipole_moment();
            scope(exit) delete_double_container(container);
            
            auto msg = container.get_error_msg.to!string;
            if(msg != "") {throw new CoolPropException(msg);}
            auto result = container.get_value();
            
            return result;
        }

        /// Get the mole fractions of the equilibrium liquid phase
        double[] mole_fractions_liquid() const
        {
            auto container = _state.mole_fractions_liquid();
            scope(exit) delete_vector_double_container(container);

            auto result = new double[](container.get_length());
            foreach(i, elem; container.get_data[0..container.get_length()])
            {
                result[i] = elem;
            }

            return result;
        }

        /// Get the mole fractions of the equilibrium vapor phase
        double[] mole_fractions_vapor() const
        {
            auto container = _state.mole_fractions_vapor();
            scope(exit) delete_vector_double_container(container);
            
            auto result = new double[](container.get_length());
            foreach(i, elem; container.get_data[0..container.get_length()])
            {
                result[i] = elem;
            }
            return result;
        }
    }

    double keyed_output(parameters key) const {return _state.keyed_output(key);}
    double trivial_keyed_output(parameters key) const
    {
        return _state.trivial_keyed_output(key);
    }

    double saturated_liquid_keyed_output(parameters key) const
    {
        return _state.saturated_liquid_keyed_output(key);
    }

    double saturated_vapor_keyed_output(parameters key) const
    {
        return _state.saturated_vapor_keyed_output(key);
    }

    double fugacity_coefficient(ulong i) const {return _state.fugacity_coefficient(i);}
    double fugacity(ulong i) const {return _state.fugacity(i);}
    double chemical_potential(ulong i) const {return _state.chemical_potential(i);}

    double first_partial_deriv(parameters Of, parameters Wrt, parameters Constant) const
    {
        auto container =  _state.first_partial_deriv(Of, Wrt, Constant);
        scope(exit) delete_double_container(container);

        auto msg = container.get_error_msg.to!string;
        if (msg != "") throw new CoolPropException(msg);

        auto result = container.get_value();

        return result;
    }

    double second_partial_deriv(parameters Of,
                                parameters Wrt1,
                                parameters Constant1, 
                                parameters Wrt2,
                                parameters Constant2) const
    {
        auto container = _state.second_partial_deriv(Of, Wrt1, Constant1, Wrt2, Constant2);
        scope(exit) delete_double_container(container);

        auto msg = container.get_error_msg.to!string;
        if (msg != "") throw new CoolPropException(msg);

        auto result = container.get_value();

        return result;
    }

    double first_saturation_deriv(parameters Of, parameters Wrt1) const
    {
        auto container = _state.first_saturation_deriv(Of, Wrt1);
        scope(exit) delete_double_container(container);

        auto msg = container.get_error_msg.to!string;
        if (msg != "") throw new CoolPropException(msg);

        auto result = container.get_value();

        return result;
    }

    double second_saturation_deriv(parameters Of, parameters Wrt1, parameters Wrt2) const
    {
        auto container = _state.second_saturation_deriv(Of, Wrt1, Wrt2);
        scope(exit) delete_double_container(container);

        auto msg = container.get_error_msg.to!string;
        if (msg != "") throw new CoolPropException(msg);

        auto result = container.get_value();

        return result;
    }

    double first_two_phase_deriv(parameters Of, parameters Wrt, parameters Constant) const
    {
        auto container = _state.first_two_phase_deriv(Of, Wrt, Constant);
        scope(exit) delete_double_container(container);

        auto msg = container.get_error_msg.to!string;
        if (msg != "") throw new CoolPropException(msg);

        auto result = container.get_value();

        return result;
    }

    double second_two_phase_deriv(parameters Of,
                                  parameters Wrt1, 
                                  parameters Constant1,
                                  parameters Wrt2,
                                  parameters Constant2) const
    {
        auto container =  _state.second_two_phase_deriv(Of, Wrt1, Constant1, Wrt2, Constant2);
        scope(exit) delete_double_container(container);

        auto msg = container.get_error_msg.to!string;
        if (msg != "") throw new CoolPropException(msg);

        auto result = container.get_value();

        return result;
    }

    double first_two_phase_deriv_splined(parameters Of,
                                         parameters Wrt, 
                                         parameters Constant,
                                         double x_end) const
    {
        auto container = _state.first_two_phase_deriv_splined(Of, Wrt, Constant, x_end);
        scope(exit) delete_double_container(container);

        auto msg = container.get_error_msg.to!string;
        if (msg != "") throw new CoolPropException(msg);

        auto result = container.get_value();

        return result;
    }

    string backend_name() const {return _state.backend_name.to!string;}

    bool clear() {return _state.clear();}

    void specify_phase(phases phase)
    {
        auto err_msg = _state.specify_phase(phase).to!string;
        if(err_msg != "") {throw new CoolPropException(err_msg);}
    }

    void unspecify_phase()
    {
        auto err_msg = _state.unspecify_phase().to!string;
        if(err_msg != "") {throw new CoolPropException(err_msg);}
    }

    /// \brief Change the equation of state for a given component to a specified EOS
    /// @param i Index of the component to change (if a pure fluid, i=0)
    /// @param EOS_name Name of the EOS to use (something like "SRK", "PR", "XiangDeiters", but backend-specific)
    /// \note Calls the calc_change_EOS function of the implementation
    void change_EOS(ulong i, string EOS_name)
    {
        auto err_msg = _state.change_EOS(i, EOS_name.toStringz).to!string;
        if(err_msg != "") {throw new CoolPropException(err_msg);}
    }

    void set_mole_fractions(double[] mole_fractions)
    {
        auto err_msg 
            = _state
            .set_mole_fractions(mole_fractions.ptr, mole_fractions.length)
            .to!string;
        if(err_msg != "") {throw new CoolPropException(err_msg);}
    }
    
    void set_mass_fractions(double[] mass_fractions)
    {
        auto err_msg 
            = _state
            .set_mass_fractions(mass_fractions.ptr, mass_fractions.length)
            .to!string;
        if(err_msg != "") {throw new CoolPropException(err_msg);}
    }
    
    void set_volu_fractions(double[] volu_fractions)
    {
        auto err_msg 
            = _state
            .set_volu_fractions(volu_fractions.ptr, volu_fractions.length)
            .to!string;
        if(err_msg != "") {throw new CoolPropException(err_msg);}
    }

    /**
     * \brief Set the reference state based on a string representation
     * @param reference_state The reference state to use, one of
     * Reference State | Description
     * -------------   | -------------------
     * "IIR"           | h = 200 kJ/kg, s=1 kJ/kg/K at 0C saturated liquid
     * "ASHRAE"        | h = 0, s = 0 @ -40C saturated liquid
     * "NBP"           | h = 0, s = 0 @ 1.0 bar saturated liquid
     * "DEF"           | Reset to the default reference state for the fluid
     * "RESET"         | Remove the offset
     * The offset in the ideal gas Helmholtz energy can be obtained from
     * \f[
     * \displaystyle\frac{\Delta s}{R_u/M}+\frac{\Delta h}{(R_u/M)T}\tau
     * \f]
     * where \f$ \Delta s = s-s_{spec} \f$ and \f$ \Delta h = h-h_{spec} \f$
     */
    void set_reference_stateS(string reference_state)
    {
        auto err_msg = _state.set_reference_stateS(reference_state.toStringz).to!string;
        if(err_msg != "") {throw new CoolPropException(err_msg);}
    }
    
    /// Set the reference state based on a thermodynamic state point specified by temperature and molar density
    /// @param T Temperature at reference state [K]
    /// @param rhomolar Molar density at reference state [mol/m^3]
    /// @param hmolar0 Molar enthalpy at reference state [J/mol]
    /// @param smolar0 Molar entropy at reference state [J/mol/K]
    void set_reference_stateD(double T,
                              double rhomolar, 
                              double hmolar0,
                              double smolar0)
    {
        auto err_msg = _state.set_reference_stateD(T, rhomolar, hmolar0, smolar0)
            .to!string;
        if(err_msg != "") {throw new CoolPropException(err_msg);}
    }

    /// Calculate the "true" critical point for pure fluids where 
    /// dpdrho|T and d2p/drho2|T are equal to zero
    void true_critical_point(double T, double rho) const
    {
        auto err_msg = _state.true_critical_point(T, rho).to!string;
        if(err_msg != "") {throw new CoolPropException(err_msg);}
    }

    void ideal_curve(string type, double[] T, double[] p) const
    {
        auto err_msg = _state.ideal_curve(type.toStringz, T.ptr, T.length, 
            p.ptr, p.length).to!string;
        if(err_msg != "") {throw new CoolPropException(err_msg);}
    }

    void viscosity_contributions(double dilute,
                                 double initial_density,
                                 double residual,
                                 double critical) const
    {
        auto err_msg = _state.viscosity_contributions(dilute, initial_density,
            residual, critical).to!string;
        if(err_msg != "") {throw new CoolPropException(err_msg);}
    }

    void conductivity_contributions(double dilute,
                                    double initial_density,
                                    double residual,
                                    double critical) const
    {
        auto err_msg = _state.conductivity_contributions(dilute, initial_density,
            residual, critical).to!string;
        if(err_msg != "") {throw new CoolPropException(err_msg);}
    }

    /** 
     * @brief Find the conformal state needed for ECS
     * @param reference_fluid The reference fluid for which the conformal state will be calculated
     * @param T Temperature (initial guess must be provided, or < 0 to start with unity shape factors)
     * @param rhomolar Molar density (initial guess must be provided, or < 0 to start with unity shape factors)
     */
    void conformal_state(string reference_fluid, double T, double rhomolar)  const
    {
        auto err_msg = _state.conformal_state(reference_fluid.toStringz, T, rhomolar).to!string;
        if(err_msg != "") {throw new CoolPropException(err_msg);}
    }

    /**
     * Convinience method for a keyed update
     */
    void keyedUpdate(parameters input1, double value1, parameters input2, double value2)
    {
        import std.format;
        
        double out1, out2;
        auto result = generate_update_pair(input1, value1, input2, value2, out1, out2);

        // Invalid input pair breakout
        if (result == input_pairs.INPUT_PAIR_INVALID)
        {
            throw new CoolPropException("Invalid input pair %s, %s".format(input1.to!string, input2.to!string));
        }

        // Now call the update method using that pair
        update(result, out1, out2);
    }
}

/**
 * Returns a simple value which does not depend upon thermodynamic state
 *
 * Parameters:
 *    fluidName: the fluid, as a string
 *    output: the simple fluid property
 *
 */
double Props1SI(string fluidName, string output)
{
    auto container = _Props1SI(fluidName.toStringz, output.toStringz);
    scope(exit) delete_double_container(container);

    auto msg = container.get_error_msg.to!string;
    if(msg != "") {throw new CoolPropException(msg);}
    auto result = container.get_value();

    return result; 
}

/**
 * Returns a value which depends upon thermodynamic state
 *
 * Parameters:
 *    output: output property, as a string
 *    name1: input thermodynamic property 1 as a string
 *    prop1: input thermodynamic property 1 value
 *    name2: input thermodynamic property 2 as a string
 *    prop2: input thermodynamic property 2 value
 *    fluidName: The fluid idenitifier, as a string
*/
double PropsSI(string output,
               string name1,
               double prop1,
               string name2,
               double prop2,
               string fluidName)
{
    auto container = _PropsSI(output.toStringz, name1.toStringz, prop1, 
        name2.toStringz, prop2, fluidName.toStringz);
    scope(exit) delete_double_container(container);

    auto msg = container.get_error_msg.to!string;
    if(msg != "") {throw new CoolPropException(msg);}
    auto result = container.get_value();

    return result; 
}

/** Extracts a value from the saturation ancillary
 * 
 * Parameters:
 *     output: Desired output variable
 *     fluidName: The fluid to be used
 *     Q: quality, 0 or 1
 *     T: input variable
 *     value: input value
 */
double saturation_ancillary(string fluidName, string output, int Q, string input, double value)
{
    auto container = _saturation_ancillary(fluidName.toStringz, output.toStringz, 
        Q, input.toStringz, value);
    scope(exit) delete_double_container(container);

    auto msg = container.get_error_msg.to!string;
    if(msg != "") {throw new CoolPropException(msg);}
    auto result = container.get_value();

    return result; 
}

/**
 * Return the fluid phase as a string
 * 
 * Parameters:
 *     name1: first state property string
 *     prop1: first state property value
 *     name2: second state property string
 *     prop2: second state property value
 * 
 * Returns:
 *     fluid phase as a string
*/
string PhaseSI(string name1, double prop1, string name2, double prop2, string fluidName)
{
    // necessary to cast c style char* to d-string
    auto result = _PhaseSI(name1.toStringz, prop1, name2.toStringz, prop2, 
        fluidName.toStringz).to!string;
    if(result.startsWith("ERROR:"))
    {
        throw new CoolPropException(result.replace("ERROR:",""));
    }
    return result;
}

/**
 * Generates an input pair from key/value pairs
 */
input_pairs generate_update_pair(parameters key1,
                                 double value1,
                                 parameters key2,
                                 double value2,
                                 ref double out1,
                                 ref double out2)
{
    return cast(input_pairs) _generate_update_pair(cast(int) key1, value1, cast(int) key2, value2, out1, out2);
}


/// High Level interface unit Tests , checking for correct interfacing of wrapper
unittest
{
    assert(Props1SI("CO2", "Tcrit") == 304.1282);
    //assertThrown!CoolPropException(Props1SI("NeutronStarMatter", "Tcrit"));
}

unittest
{
    assert(PropsSI("D","P", 8.0E6, "T", 400, "CO2").approxEqual(123.904893));
    assertThrown!CoolPropException(PropsSI("D", "P", 8.0E6, "T", -400, "CO2"));
}

unittest
{
    assert(saturation_ancillary("CO2", "P", 1, "T", 250).approxEqual(1_785_043.817_482));
    assertThrown!CoolPropException(saturation_ancillary("CO2","P",-1,"T",250));
}

unittest
{
    assert(PhaseSI("P", 6.0E6, "T", 250, "CO2") == "liquid");
    assert(PhaseSI("P", 6.0E6, "T", 300, "CO2") == "gas");
    assert(PhaseSI("P", 8.0E6, "T", 500, "CO2") == "supercritical");
    assertThrown!CoolPropException(PhaseSI("P",0.0, "T", 400, "CO2"));
}

// Test generate_update_pair
unittest
{
    // Valid input pair
    double out1, out2;
    auto result = generate_update_pair(parameters.iT,
                                       300.0,
                                       parameters.iP,
                                       8.0E6,
                                       out1,
                                       out2);

    assert(result == input_pairs.PT_INPUTS);
    assert(out1 == 8.0E6);
    assert(out2 == 300.0);

    // Invalid output pair
    double out3, out4;
    auto result2 = generate_update_pair(parameters.iQ,
                                       0.5,
                                       parameters.iSmass,
                                       3000.0,
                                       out3,
                                       out4);

    assert(result2 == input_pairs.INPUT_PAIR_INVALID);
}

/// Low Level Interface Tests

// Testing abstract state update and fields
unittest
{
    auto fluid = new AbstractState("HEOS", "CO2");

    with(fluid)
    {
        assert(name == "CarbonDioxide");
        assert(backend_name == "HelmholtzEOSBackend");

        specify_phase(iphase_supercritical);
        assert(phase == iphase_supercritical);
        unspecify_phase();

        update(PT_INPUTS, 8.0E6, 400.0);
        
        assert(T.approxEqual(400.0 ));
        assert(p.approxEqual(8.0E6 ));
        assert(rhomolar.approxEqual(2815.393241) );
        assert(rhomass.approxEqual(123.904893));
        assert(Q == -1.0 );
        assert(tau.approxEqual(0.7603205) );
        assert(delta.approxEqual(0.2649805242) );
        assert(molar_mass.approxEqual(0.0440098) );
        assert(acentric_factor.approxEqual(0.22394) );
        assert(gas_constant.approxEqual(8.31451) );
        assert(Bvirial.approxEqual(-6.02694679E-5) );
        assert(Cvirial.approxEqual(3.300685e-09) );
        assert(compressibility_factor.approxEqual(p*molar_mass/(rhomass*gas_constant*T)));
        assert(hmolar.approxEqual(24387.060886) );
        assert(hmass.approxEqual(554127.96436) );
        assert(hmolar_excess.approxEqual(8.815914e-07) );
        assert(hmass_excess.approxEqual(2.003171e-05) );
        assert(smolar.approxEqual(92.151205) );
        assert(smass.approxEqual(2093.87921) );
        assert(smolar_excess.approxEqual(5.210921e-09) );
        assert(smass_excess.approxEqual(1.1840366173212698e-07) );
        assert(umolar.approxEqual(21545.539537) );
        assert(umass.approxEqual(489562.314) );
        assert(umolar_excess.approxEqual(6.800765e-07) );
        assert(umass_excess.approxEqual(1.545284e-05) );
        assert(cpmolar.approxEqual(54.03095) );
        assert(cpmass.approxEqual(1227.70257) );
        assert(cp0molar.approxEqual(41.3339011) );
        assert(cp0mass.approxEqual(939.197659) );
        assert(cvmolar.approxEqual(35.7584116) );
        assert(cvmass.approxEqual(812.5102054) );
        assert(gibbsmolar.approxEqual(-12473.4212227) );
        assert(gibbsmass.approxEqual(-283423.719779) );
        assert(gibbsmolar_excess.approxEqual(-1.20278e-06) );
        assert(gibbsmass_excess.approxEqual(-2.7329858e-05) );
        assert(helmholtzmolar.approxEqual(-15314.94257) );
        assert(helmholtzmass.approxEqual(-347989.3699) );
        assert(helmholtzmolar_excess.approxEqual(-1.404292e-06) );
        assert(helmholtzmass_excess.approxEqual(-3.1908623e-05) );
        assert(volumemolar_excess.approxEqual(1.75535855e-13) );
        assert(volumemass_excess.approxEqual(3.988562897e-12) );
        assert(speed_sound.approxEqual(289.06614047) );
        assert(isothermal_compressibility.approxEqual(1.4594238e-07) );
        assert(isobaric_expansion_coefficient.approxEqual(0.0043324108) );

        assert(Tmin.approxEqual(216.592) );
        assert(Tmax.approxEqual(2000.0) );
        assert(pmax.approxEqual(800000000.0) );
        assert(Ttriple.approxEqual(216.592) );
        assert(T_critical.approxEqual(304.1282) );
        assert(p_critical.approxEqual(7377300.0) );
        assert(rhomolar_critical.approxEqual(10624.9063) );
        assert(rhomass_critical.approxEqual(467.60000128) );
        assert(T_reducing.approxEqual(304.1282) );
        assert(rhomolar_reducing.approxEqual(10624.9063) );
        assert(rhomass_reducing.approxEqual(467.6000012817) );
        assert(p_triple.approxEqual(517964.0) );

        assert(trivial_keyed_output(iT_triple).approxEqual(216.592));
        assert(keyed_output(iDmass).approxEqual(123.904893));

        assert(first_partial_deriv(iHmass, iT, iP).approxEqual(cpmass));
        assert(second_partial_deriv(iHmass, iT, iP, iDmass, iT).approxEqual(2.709880340431038));

        assert(alpha0.approxEqual(-4.447482161442656) );
        assert(dalpha0_dDelta.approxEqual(3.7738622598970455) );
        assert(dalpha0_dTau.approxEqual(9.088487030205455) );
        assert(d2alpha0_dDelta2.approxEqual(-14.242036356675236) );
        assert(d2alpha0_dDelta_dTau.approxEqual(0.0) );
        assert(d2alpha0_dTau2.approxEqual(-6.869719432912817) );
        assert(d3alpha0_dTau3.approxEqual(22.082963729336505) );
        assert(d3alpha0_dDelta_dTau2.approxEqual(0.0) );
        assert(d3alpha0_dDelta2_dTau.approxEqual(0.0) );
        assert(d3alpha0_dDelta3.approxEqual(107.49496702107659) );
        assert(alphar.approxEqual(-0.15740212261842768) );
        assert(dalphar_dDelta.approxEqual(-0.5495260757387017) );
        assert(dalphar_dTau.approxEqual(-0.5680077928080228) );
        assert(d2alphar_dDelta2.approxEqual(0.32772798909153794) );
        assert(d2alphar_dDelta_dTau.approxEqual(-2.053749164891351) );
        assert(d2alphar_dTau2.approxEqual(-0.5698561276259745) );
        assert(d3alphar_dDelta3.approxEqual(-0.0502223743538671) );
        assert(d3alphar_dDelta2_dTau.approxEqual(0.6921842488676662) );
        assert(d3alphar_dDelta_dTau2.approxEqual(-1.9270602445702312) );
        assert(d3alphar_dTau3.approxEqual(-0.836654188126867) );
        assert(d4alphar_dDelta4.approxEqual(0.30861400140197826) );
        assert(d4alphar_dDelta3_dTau.approxEqual(-0.07561009345300576) );
        assert(d4alphar_dDelta2_dTau2.approxEqual(2.0916609478129797) );
        assert(d4alphar_dDelta_dTau3.approxEqual(-3.066305368662974) );
        assert(d4alphar_dTau4.approxEqual(-2.282187347341857) );
        assert(dBvirial_dT.approxEqual(3.98774e-07));
        assert(dCvirial_dT.approxEqual(-1.00312e-11));
        assert(viscosity.approxEqual(2.12999e-05));
        assert(conductivity.approxEqual(0.0295146));
        assert(Prandtl.approxEqual(0.886001));
        assert(fundamental_derivative_of_gas_dynamics.approxEqual(1.18679));
        assert(PIP.approxEqual(0.526076));
        assert(mole_fractions_liquid == [1.0]);
        assert(mole_fractions_vapor == [1.0]);

        // attributes not implemented for this backend-fluid
        assertThrown!CoolPropException(dipole_moment);
        assertThrown!CoolPropException(surface_tension);
        
        clear();

        change_EOS(0, "SRK");
        //assert(backend_name == "SRKBackend"); // bug #1702       
    }
}

// Test AbstractState keyedInput method
unittest
{
    auto fluid = new AbstractState("HEOS", "CO2");

    // Valid keyed input
    fluid.keyedUpdate(parameters.iT, 400.0, parameters.iP, 8.0E6);

    assert(fluid.T.approxEqual(400.0 ));
    assert(fluid.p.approxEqual(8.0E6 ));
    assert(fluid.rhomolar.approxEqual(2815.393241) );
    assert(fluid.rhomass.approxEqual(123.904893));
    // Etc

    // Invalid keyed input
    assertThrown!(CoolPropException)(fluid.keyedUpdate(parameters.iDmolar, 2815.393241,
                                                       parameters.iSmass, 2093.87921));
}

// Test AbstractState call with invalid inputs, should catch and rethrow exception
unittest
{
    assertThrown!CoolPropException(new AbstractState("foo", "CO2"));
    assertThrown!CoolPropException(new AbstractState("HEOS", "NeutronStarMatter"));

    auto fluid = new AbstractState("HEOS", "CO2");
    assertThrown!CoolPropException(fluid.update(PT_INPUTS, -1.0E6, 400));

    assertThrown!CoolPropException(fluid.change_EOS(1, "SRK"));
    //assertThrown!CoolPropException(fluid.change_EOS(0, "EquationOfEverything")); // allows silent errors

}

// Test duplication
unittest
{
    auto original = new AbstractState("HEOS", "CO2");
    original.update(PT_INPUTS, 8.0E6, 400);

    auto duplicate = original.dup;

    assert(original.rhomass.approxEqual(duplicate.rhomass));
    original.update(PT_INPUTS, 20E6, 500);
    assert(!original.rhomass.approxEqual(duplicate.rhomass));
}

// Surface Tension
unittest
{
    auto fluid = new AbstractState("HEOS", "CO2");
    fluid.update(PQ_INPUTS, 7.2E6, 0.8);
    assert(fluid.surface_tension.approxEqual(6.51850079427795e-05));
}
