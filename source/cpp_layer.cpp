/*
Interface Layer for CoolProp D wrapper

Acts as an intermediate layer between the D extern(C++) definitions and the 
CoolProp source. Removes the CoolProp namespace and casts all C++ types to
directly interfacable d types
*/

#include "CoolProp.h"
#include "AbstractState.h"
#include "DataStructures.h"
#include <memory>
#include <string>
#include <iostream>

/// Stores a double result, or an error message. Used to propogate exceptions 
/// back to D
class DoubleContainer
{
    private:
        std::string err_msg = "";
        double val;

    public:

        DoubleContainer(double val)
        {
            this->val = val;
        }

        DoubleContainer(std::string err_msg)
        {
            if(err_msg != "")
            {
                this->err_msg = err_msg;
            }
            else
            {
                std::string default_msg = "No Error Message Provided"; 
                this->err_msg = default_msg;
            }
        }

        char* get_error_msg(void);
        double get_value(void);
};

// get the error message as a c string
char* DoubleContainer::get_error_msg(void)
{
    std::string s = this->err_msg;
    char *cstr = new char[s.length() + 1];
    strcpy(cstr, s.c_str());
    return cstr;
}

double DoubleContainer::get_value(void) {return this->val;};

// Destructor
void delete_double_container(DoubleContainer *&inst)
{
    delete inst;
    inst = 0;
}


class VectorDoubleContainer
{
    private:
        std::vector<double> val;
        std::string err_msg;

    public:
        VectorDoubleContainer(std::vector<double> val)
        {
            this->val = val;
        }    

        VectorDoubleContainer(std::string err_msg)
        {
            if(err_msg !="")
            {
                this->err_msg = err_msg;
            }
            else
            {
                std::string default_msg = "No Error Message Provided";
                this->err_msg = default_msg;
            }
        }

        char* get_error_msg(void);
        double* get_data(void);
        unsigned long long get_length(void);
};

// get the error message as a c string
char* VectorDoubleContainer::get_error_msg(void)
{
    std::string s = this->err_msg;
    char *cstr = new char[s.length() + 1];
    strcpy(cstr, s.c_str());
    return cstr;
}

double* VectorDoubleContainer::get_data(void) {return this->val.data();};
unsigned long long VectorDoubleContainer::get_length(void) {return this->val.size();};

// Destructor
void delete_vector_double_container(VectorDoubleContainer *&inst)
{
    delete inst;
    inst = 0;
}

/* -------------------- High Level interface calls -------------------------- */

int _generate_update_pair(int key1, double value1, int key2, double value2, double& out1, double& out2)
{
    return static_cast<int> (CoolProp::generate_update_pair<double>(static_cast<CoolProp::parameters>(key1),
                                                                    value1,
                                                                    static_cast<CoolProp::parameters>(key2),
                                                                    value2,
                                                                    out1,
                                                                    out2));
}


DoubleContainer* _Props1SI(const char* fluidName, const char* output)
{
    double val;
    try
    {
        val = CoolProp::Props1SI(fluidName, output);
        return new DoubleContainer(val);
    }
    catch(const std::exception& e)
    {
        return new DoubleContainer(e.what() );
    }
}

DoubleContainer* _PropsSI(const char* output,
                          const char* name1, 
                          double prop1,
                          const char* name2,
                          double prop2,
                          const char* fluidName)
{
    double val;
    try
    {
        val = CoolProp::PropsSI(output, name1, prop1, name2, prop2, fluidName);
        return new DoubleContainer(val);
    }
    catch(const std::exception& e)
    {
        return new DoubleContainer(e.what() );   
    }
}

DoubleContainer* _saturation_ancillary(const char* fluidName, const char* output, 
    int Q, const char* input, double value)
{
    try
    {
        return new DoubleContainer(CoolProp::saturation_ancillary(fluidName, output, Q, input, value));   
    }
    catch(const std::exception &e)
    {
        return new DoubleContainer(e.what() );
    }
}

char* _PhaseSI(const char* name1, double prop1, const char* name2,
    double prop2, const char* fluidName) 
{
    std::string result;
    try 
    {
        result = CoolProp::PhaseSI(name1, prop1, name2, prop2, fluidName);
    }
    catch(const std::exception& e)
    {
        result = e.what();
        std::string error_id = "ERROR:";
        result.insert(0, error_id);
    }
    char *cstr = new char[result.length() + 1];
    strcpy(cstr, result.c_str());

    return cstr;
}

char* get_global_param_string(const char* ParamName)
{
    std::string s = get_global_param_string(ParamName);

    char *cstr = new char[s.length() + 1];
    strcpy(cstr, s.c_str());

    return cstr;
}

/* ------------------------- Low Level Interface ---------------------------- */
class _AbstractState
/** 
Defines the translation layer interface between C++ and D with thin wrappers
over all public methods
*/
{
    private:
        // Stores the constructor failure message for D to throw
        std::string error_msg = "";

    public:

        // actual AbstractState instance
        std::unique_ptr<CoolProp::AbstractState> _state;

        // 'Failure' constructor, stores the error message
        _AbstractState(const char* err_msg) {this->error_msg = err_msg;};

        // 'Success' constructor, implements _state
        _AbstractState(const char* backend, const char* fluidName)
        {
            _state.reset(CoolProp::AbstractState::factory(backend, fluidName));
        };

        // get the error message as a c string
        const char* get_error_msg(void) const;

        // call _state.update and return c_string "" if successful else error message
        const char* update(int input_pair, double value1, double value2);

        double T(void) const;
        double rhomolar(void) const;
        double rhomass(void) const;
        double p(void) const;
        double Q(void) const;
        double tau(void) const;
        double delta(void) const;
        double molar_mass(void) const;
        double acentric_factor(void) const;
        double gas_constant(void) const;
        double Bvirial(void) const;
        double dBvirial_dT(void) const;
        double Cvirial(void) const;
        double dCvirial_dT(void) const;
        double compressibility_factor(void) const;
        double hmolar(void) const;
        double hmass(void) const;
        double hmolar_excess(void) const;
        double hmass_excess(void) const;
        double smolar(void) const;
        double smass(void) const;
        double smolar_excess(void) const;
        double smass_excess(void) const;
        double umolar(void) const;
        double umass(void) const;
        double umolar_excess(void) const;
        double umass_excess(void) const;
        double cpmolar(void) const;
        double cpmass(void) const;
        double cp0molar(void) const;
        double cp0mass(void) const;
        double cvmolar(void) const;
        double cvmass(void) const;
        double gibbsmolar(void) const;
        double gibbsmass(void) const;
        double gibbsmolar_excess(void) const;
        double gibbsmass_excess(void) const;
        double helmholtzmolar(void) const;
        double helmholtzmass(void) const;
        double helmholtzmolar_excess(void) const;
        double helmholtzmass_excess(void) const;
        double volumemolar_excess(void) const;
        double volumemass_excess(void) const;
        double speed_sound(void) const;
        double isothermal_compressibility(void) const;
        double isobaric_expansion_coefficient(void) const;
        double fugacity_coefficient(unsigned long i) const;
        double fugacity(unsigned long i) const;
        double chemical_potential(unsigned long i) const;
        const char* backend_name(void) const;
        double Tmin(void) const;
        double Tmax(void) const;
        double pmax(void) const;
        double Ttriple(void) const;
        int phase(void) const;
        const char* specify_phase(int phase);
        const char* unspecify_phase(void);
        double T_critical(void) const;
        double p_critical(void) const;
        double rhomolar_critical(void) const;
        double rhomass_critical(void) const;
        double T_reducing(void) const;
        double rhomolar_reducing(void) const;
        double rhomass_reducing(void) const;
        double p_triple(void) const;
        const char* name() const;
        const DoubleContainer* dipole_moment() const;
        double keyed_output(int key) const;
        double trivial_keyed_output(int key) const;
        double saturated_liquid_keyed_output(int key) const;
        double saturated_vapor_keyed_output(int key) const;
        double viscosity(void) const;
        double conductivity(void) const;
        double Prandtl(void) const;

        double fundamental_derivative_of_gas_dynamics(void) const;
        double PIP() const;
        const DoubleContainer* surface_tension(void) const;
        const DoubleContainer* first_two_phase_deriv(int Of, int Wrt, int Constant) const;
        const DoubleContainer* second_two_phase_deriv(int Of,
                                                      int Wrt1,
                                                      int Constant1,
                                                      int Wrt2,
                                                      int Constant2) const;
    
        const DoubleContainer* first_two_phase_deriv_splined(int Of, int Wrt, int Constant, double x_end) const;

        const DoubleContainer* first_partial_deriv(int Of, int Wrt, int Constant) const;
        const DoubleContainer* second_partial_deriv(int Of1,
                                                    int Wrt1,
                                                    int Constant1,
                                                    int Wrt2,
                                                    int Constant2) const;
        const DoubleContainer* first_saturation_deriv(int Of1, int Wrt1) const;
        const DoubleContainer* second_saturation_deriv(int Of1, int Wrt1, int Wrt2) const;
        double alpha0(void) const;
        double dalpha0_dDelta(void) const;
        double dalpha0_dTau(void) const;
        double d2alpha0_dDelta2(void) const;
        double d2alpha0_dDelta_dTau(void) const;
        double d2alpha0_dTau2(void) const;
        double d3alpha0_dTau3(void) const;
        double d3alpha0_dDelta_dTau2(void) const;
        double d3alpha0_dDelta2_dTau(void) const;
        double d3alpha0_dDelta3(void) const;
        double alphar(void) const;
        double dalphar_dDelta(void) const;
        double dalphar_dTau(void) const;
        double d2alphar_dDelta2(void) const;
        double d2alphar_dDelta_dTau(void) const;
        double d2alphar_dTau2(void) const;
        double d3alphar_dDelta3(void) const;
        double d3alphar_dDelta2_dTau(void) const;
        double d3alphar_dDelta_dTau2(void) const;
        double d3alphar_dTau3(void) const;
        double d4alphar_dDelta4(void) const;
        double d4alphar_dDelta3_dTau(void) const;
        double d4alphar_dDelta2_dTau2(void) const;
        double d4alphar_dDelta_dTau3(void) const;
        double d4alphar_dTau4(void) const;

        const char* set_mole_fractions(double* mole_fractions, unsigned long size);
        const char* set_mass_fractions(double* mass_fractions, unsigned long size);
        const char* set_volu_fractions(double* volu_fractions, unsigned long size);
        const char* set_reference_stateS(const char* reference_state);
        const char* set_reference_stateD(double T, double rhomolar, double hmolar0, double smolar0);
        const VectorDoubleContainer* mole_fractions_liquid(void) const;
        const VectorDoubleContainer* mole_fractions_vapor(void) const;
        const char* true_critical_point(double T, double rho) const;
        const char* ideal_curve(const char* type,
                                const double* T,
                                unsigned long T_length,
                                const double* p,
                                unsigned long p_length) const;
    
        const char* viscosity_contributions(double dilute,
                                            double initial_density,
                                            double residual,
                                            double critical) const;
    
        const char* conductivity_contributions(double dilute,
                                               double initial_density,
                                               double residual,
                                               double critical) const;
    
        const char* conformal_state(const char* reference_fluid,
                                    double T,
                                    double rhomolar) const;
    
        const char* change_EOS(const unsigned long i,
                               const char* EOS_name);
        bool clear();
};

const char* _AbstractState::update(int input_pair, double value1, double value2)
{
    // if update successful, return empty c string
    try 
    {
        this->_state->update(static_cast<CoolProp::input_pairs>(input_pair), 
            value1, value2);
        return (char*)"";
    } 
    // else propogate the exception message to D to throw
    catch (const std::exception& e) 
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }
}

// get the error message as a c string
const char* _AbstractState::get_error_msg(void) const
{
    std::string s = this->error_msg;
    char *cstr = new char[s.length() + 1];
    strcpy(cstr, s.c_str());
    return cstr;
}

double _AbstractState::T(void) const { return this->_state->T();};
double _AbstractState::rhomolar(void) const { return this->_state->rhomolar(); };
double _AbstractState::rhomass(void) const { return this->_state->rhomass(); };
double _AbstractState::p(void) const   {return this->_state->p();};
double _AbstractState::Q(void) const   {return this->_state->Q();};
double _AbstractState::tau(void) const  {return this->_state->tau();};
double _AbstractState::delta(void) const  {return this->_state->delta();};
double _AbstractState::molar_mass(void) const  {return this->_state->molar_mass();};
double _AbstractState::acentric_factor(void) const  {return this->_state->acentric_factor();};
double _AbstractState::gas_constant(void) const  {return this->_state->gas_constant();};
double _AbstractState::Bvirial(void) const  {return this->_state->Bvirial();};
double _AbstractState::Cvirial(void) const  {return this->_state->Cvirial();};
double _AbstractState::compressibility_factor(void) const  {return this->_state->compressibility_factor();};
double _AbstractState::hmolar(void) const  {return this->_state->hmolar();};
double _AbstractState::hmass(void) const  {return this->_state->hmass();};
double _AbstractState::hmolar_excess(void) const  {return this->_state->hmolar_excess();};
double _AbstractState::hmass_excess(void) const   {return this->_state->hmass_excess();};
double _AbstractState::smolar(void) const  {return this->_state->smolar();};
double _AbstractState::smass(void) const  {return this->_state->smass();};
double _AbstractState::smolar_excess(void) const  {return this->_state->smolar_excess();};
double _AbstractState::smass_excess(void) const   {return this->_state->smass_excess();};
double _AbstractState::umolar(void) const  {return this->_state->umolar();};
double _AbstractState::umass(void) const  {return this->_state->umass();};
double _AbstractState::umolar_excess(void) const  {return this->_state->umolar_excess();};
double _AbstractState::umass_excess(void) const  {return this->_state->umass_excess();};
double _AbstractState::cpmolar(void) const  {return this->_state->cpmolar();};
double _AbstractState::cpmass(void) const  {return this->_state->cpmass();};
double _AbstractState::cp0molar(void) const  {return this->_state->cp0molar();};
double _AbstractState::cp0mass(void) const  {return this->_state->cp0mass();};
double _AbstractState::cvmolar(void) const  {return this->_state->cvmolar();};
double _AbstractState::cvmass(void) const  {return this->_state->cvmass();};
double _AbstractState::gibbsmolar(void) const  {return this->_state->gibbsmolar();};
double _AbstractState::gibbsmass(void) const  {return this->_state->gibbsmass();};
double _AbstractState::gibbsmolar_excess(void) const  {return this->_state->gibbsmolar_excess();};
double _AbstractState::gibbsmass_excess(void) const  {return this->_state->gibbsmass_excess();};
double _AbstractState::helmholtzmolar(void) const  {return this->_state->helmholtzmolar();};
double _AbstractState::helmholtzmass(void) const  {return this->_state->helmholtzmass();};
double _AbstractState::helmholtzmolar_excess(void) const  {return this->_state->helmholtzmolar_excess();};
double _AbstractState::helmholtzmass_excess(void) const  {return this->_state->helmholtzmass_excess();};
double _AbstractState::volumemolar_excess(void) const  {return this->_state->volumemolar_excess();};
double _AbstractState::volumemass_excess(void) const  {return this->_state->volumemass_excess();};
double _AbstractState::speed_sound(void) const  {return this->_state->speed_sound();};
double _AbstractState::isothermal_compressibility(void) const  {return this->_state->isothermal_compressibility();};
double _AbstractState::isobaric_expansion_coefficient(void) const {return this->_state->isobaric_expansion_coefficient();};
double _AbstractState::fugacity_coefficient(unsigned long i) const {return this->_state->fugacity_coefficient(i);};
double _AbstractState::fugacity(unsigned long i) const {return this->_state->fugacity(i);};
double _AbstractState::chemical_potential(unsigned long i) const {return this->_state->chemical_potential(i);};

bool _AbstractState::clear() {return this->_state->clear();};

double _AbstractState::Tmin(void) const {return this->_state->Tmin();};
double _AbstractState::Tmax(void) const {return this->_state->Tmax();};
double _AbstractState::pmax(void) const {return this->_state->pmax();};
double _AbstractState::Ttriple(void) const {return this->_state->Ttriple();};
double _AbstractState::T_critical(void) const {return this->_state->T_critical();};
double _AbstractState::p_critical(void) const {return this->_state->p_critical();};
double _AbstractState::rhomolar_critical(void) const {return this->_state->rhomolar_critical();};
double _AbstractState::rhomass_critical(void) const {return this->_state->rhomass_critical();};
double _AbstractState::T_reducing(void) const {return this->_state->T_reducing();};
double _AbstractState::rhomolar_reducing(void) const {return this->_state->rhomolar_reducing();};
double _AbstractState::rhomass_reducing(void) const {return this->_state->rhomass_reducing();};
double _AbstractState::p_triple(void) const {return this->_state->p_triple();};
double _AbstractState::dBvirial_dT(void) const { return this->_state->dBvirial_dT();};
double _AbstractState::dCvirial_dT(void) const { return this->_state->dCvirial_dT();};

const DoubleContainer* _AbstractState::dipole_moment() const
{
    try 
    {
        return new DoubleContainer(this->_state->dipole_moment());
    }
    catch(const std::exception& e)
    {
        return new DoubleContainer(e.what());
    }
} 

const char* _AbstractState::backend_name(void) const
{
    std::string s = this->_state->backend_name();
    char *cstr = new char[s.length() + 1];
    strcpy(cstr, s.c_str());
    return cstr;
}

const char* _AbstractState::name() const
{
    std::string s = this->_state->name();
    char *cstr = new char[s.length() + 1];
    strcpy(cstr, s.c_str());

    return cstr;
}

int _AbstractState::phase(void) const {return this->_state->phase();}

const char* _AbstractState::specify_phase(int phase)
{
    try
    {
        this->_state->specify_phase(static_cast<CoolProp::phases>(phase));
        return (char*)"";
    }
    catch(const std::exception& e)
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }
}

const char* _AbstractState::unspecify_phase(void)
{
    try
    {
        this->_state->unspecify_phase();
        return (char*)"";
    }
    catch(const std::exception& e)
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }
} 

double _AbstractState::keyed_output(int key) const
{
    return this->_state->keyed_output(static_cast<CoolProp::parameters>(key));
}

double _AbstractState::trivial_keyed_output(int key) const
{
    return this->_state->trivial_keyed_output(static_cast<CoolProp::parameters>(key));
}

double _AbstractState::saturated_liquid_keyed_output(int key) const
{
    return this->_state->saturated_liquid_keyed_output(static_cast<CoolProp::parameters>(key));
}

double _AbstractState::saturated_vapor_keyed_output(int key) const
{
    return this->_state->saturated_vapor_keyed_output(static_cast<CoolProp::parameters>(key));
}

double _AbstractState::viscosity(void) const {return this->_state->viscosity();}; 
double _AbstractState::conductivity(void) const { return this->_state->conductivity();};
double _AbstractState::Prandtl(void) const  {return this->_state->Prandtl();};
double _AbstractState::fundamental_derivative_of_gas_dynamics(void) const 
{
    return this->_state->fundamental_derivative_of_gas_dynamics();   
}
double _AbstractState::PIP(void) const { return this->_state->PIP();};
const DoubleContainer* _AbstractState::surface_tension(void) const
{
    try
    {
        return new DoubleContainer(this->_state->surface_tension());
    }
    catch (const std::exception& e)
    {
        return new DoubleContainer(e.what());
    }
};

const DoubleContainer* _AbstractState::first_two_phase_deriv(int Of, int Wrt, int Constant) const
{
    try
    {
        return new DoubleContainer(this->_state->first_two_phase_deriv(
                                       static_cast<CoolProp::parameters>(Of), 
                                       static_cast<CoolProp::parameters>(Wrt), 
                                       static_cast<CoolProp::parameters>(Constant)));
    }
    catch (std::exception e)
    {
        return new DoubleContainer(e.what());
    }
}

const DoubleContainer* _AbstractState::second_two_phase_deriv(int Of,
                                                              int Wrt1,
                                                              int Constant1,
                                                              int Wrt2,
                                                              int Constant2) const
{
    try
    {
        return new DoubleContainer(this->_state->second_two_phase_deriv(
                                       static_cast<CoolProp::parameters>(Of),
                                       static_cast<CoolProp::parameters>(Wrt1),
                                       static_cast<CoolProp::parameters>(Constant1),
                                       static_cast<CoolProp::parameters>(Wrt2),
                                       static_cast<CoolProp::parameters>(Constant2)));
    }
    catch (std::exception e)
    {
        return new DoubleContainer(e.what());
    }
}
const DoubleContainer* _AbstractState::first_two_phase_deriv_splined(int Of,
                                                                     int Wrt,
                                                                     int Constant,
                                                                     double x_end) const
{
    try
    {
        return new DoubleContainer(this->_state->first_two_phase_deriv_splined(
                                       static_cast<CoolProp::parameters>(Of), 
                                       static_cast<CoolProp::parameters>(Wrt), 
                                       static_cast<CoolProp::parameters>(Constant), 
                                       x_end));
    }
    catch (std::exception e)
    {
        return new DoubleContainer(e.what());
    }
}

const DoubleContainer* _AbstractState::first_partial_deriv(int Of, int Wrt, int Constant) const
{
    try
    {
        return new DoubleContainer(this->_state->first_partial_deriv(
                                       static_cast<CoolProp::parameters>(Of), 
                                       static_cast<CoolProp::parameters>(Wrt), 
                                       static_cast<CoolProp::parameters>(Constant)));
    }
    catch (std::exception e)
    {
        return new DoubleContainer(e.what());
    }
}
const DoubleContainer* _AbstractState::second_partial_deriv(int Of1,
                                                            int Wrt1,
                                                            int Constant1,
                                                            int Wrt2,
                                                            int Constant2) const
{
    try
    {
        return new DoubleContainer(this->_state->second_partial_deriv(
                                       static_cast<CoolProp::parameters>(Of1), 
                                       static_cast<CoolProp::parameters>(Wrt1), 
                                       static_cast<CoolProp::parameters>(Constant1), 
                                       static_cast<CoolProp::parameters>(Wrt2), 
                                       static_cast<CoolProp::parameters>(Constant2)));
    }
    catch (std::exception e)
    {
        return new DoubleContainer(e.what());
    }
}
const DoubleContainer* _AbstractState::first_saturation_deriv(int Of1, int Wrt1) const
{
    try
    {
        return new DoubleContainer(this->_state->first_saturation_deriv(
                                       static_cast<CoolProp::parameters>(Of1), 
                                       static_cast<CoolProp::parameters>(Wrt1)));
    }
    catch (std::exception e)
    {
        return new DoubleContainer (e.what());
    }
}
const DoubleContainer* _AbstractState::second_saturation_deriv(int Of1, int Wrt1, int Wrt2) const
{
    try
    {
        return new DoubleContainer( this->_state->second_saturation_deriv(
                                        static_cast<CoolProp::parameters>(Of1), 
                                        static_cast<CoolProp::parameters>(Wrt1), 
                                        static_cast<CoolProp::parameters>(Wrt2)));
    }
    catch (std::exception e)
    {
        return new DoubleContainer (e.what());
    }
}
double _AbstractState::alpha0(void) const  {return this->_state->alpha0();}
double _AbstractState::dalpha0_dDelta(void) const  {return this->_state->dalpha0_dDelta();}
double _AbstractState::dalpha0_dTau(void) const {return this->_state->dalpha0_dTau();}
double _AbstractState::d2alpha0_dDelta2(void) const {return this->_state->d2alpha0_dDelta2();}
double _AbstractState::d2alpha0_dDelta_dTau(void) const {return this->_state->d2alpha0_dDelta_dTau();}
double _AbstractState::d2alpha0_dTau2(void) const  {return this->_state->d2alpha0_dTau2();}
double _AbstractState::d3alpha0_dTau3(void) const  {return this->_state->d3alpha0_dTau3();}
double _AbstractState::d3alpha0_dDelta_dTau2(void) const {return this->_state->d3alpha0_dDelta_dTau2();}
double _AbstractState::d3alpha0_dDelta2_dTau(void) const {return this->_state->d3alpha0_dDelta2_dTau();}
double _AbstractState::d3alpha0_dDelta3(void) const {return this->_state->d3alpha0_dDelta3();}
double _AbstractState::alphar(void) const {return this->_state->alphar();}
double _AbstractState::dalphar_dDelta(void) const {return this->_state->dalphar_dDelta();}
double _AbstractState::dalphar_dTau(void) const {return this->_state->dalphar_dTau();}
double _AbstractState::d2alphar_dDelta2(void) const {return this->_state->d2alphar_dDelta2();}
double _AbstractState::d2alphar_dDelta_dTau(void) const {return this->_state->d2alphar_dDelta_dTau();}
double _AbstractState::d2alphar_dTau2(void) const {return this->_state->d2alphar_dTau2();}
double _AbstractState::d3alphar_dDelta3(void) const {return this->_state->d3alphar_dDelta3();}
double _AbstractState::d3alphar_dDelta2_dTau(void) const {return this->_state->d3alphar_dDelta2_dTau();}
double _AbstractState::d3alphar_dDelta_dTau2(void) const {return this->_state->d3alphar_dDelta_dTau2();}
double _AbstractState::d3alphar_dTau3(void) const {return this->_state->d3alphar_dTau3();}
double _AbstractState::d4alphar_dDelta4(void) const {return this->_state->d4alphar_dDelta4();}
double _AbstractState::d4alphar_dDelta3_dTau(void) const {return this->_state->d4alphar_dDelta3_dTau();}
double _AbstractState::d4alphar_dDelta2_dTau2(void) const {return this->_state->d4alphar_dDelta2_dTau2();}
double _AbstractState::d4alphar_dDelta_dTau3(void) const {return this->_state->d4alphar_dDelta_dTau3();}
double _AbstractState::d4alphar_dTau4(void) const {return this->_state->d4alphar_dTau4();}

const char* _AbstractState::set_reference_stateS(const char* reference_state)
{
    try
    {
        this->_state->set_reference_stateS(reference_state);
        return (char*)"";
    }
    catch(const std::exception& e)
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }
}

const char* _AbstractState::set_reference_stateD(double T, double rhomolar, double hmolar0, double smolar0)
{
    try
    {
        this->_state->set_reference_stateD(T, rhomolar, hmolar0, smolar0);
        return (char*)"";
    }
    catch(const std::exception& e)
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }
}

const char* _AbstractState::set_mole_fractions(double* mole_fractions, unsigned long size)
{
    try
    {
        std::vector<double> values(mole_fractions, mole_fractions + size);
        this->_state->set_mole_fractions(values);
        return (char*)"";
    }
    catch(const std::exception& e)
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }
}

const char* _AbstractState::set_mass_fractions(double* mass_fractions, unsigned long size)
{
    try 
    {
        std::vector<double> values(mass_fractions, mass_fractions + size);
        this->_state->set_mass_fractions(values);
        return (char*)"";
    }
    catch(const std::exception& e)
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }
}

const char* _AbstractState::set_volu_fractions(double* volu_fractions, unsigned long size)
{
    try
    {
        std::vector<double> values(volu_fractions, volu_fractions + size);
        this->_state->set_volu_fractions(values);
        return (char*)"";
    }
    catch(const std::exception& e)
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }
}

const char* _AbstractState::change_EOS(const unsigned long i, const char* EOS_name) 
{
    try
    {
        this->_state->change_EOS(i, EOS_name);
        return (char*)"";
    }
    catch(const std::exception& e)
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }
}

const char* _AbstractState::true_critical_point(double T, double rho) const
{
    try
    {
        this->_state->true_critical_point(T, rho);
        return (char*)"";
    }
    catch(const std::exception& e)
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }
}

const VectorDoubleContainer* _AbstractState::mole_fractions_liquid(void) const
{
    std::vector<double> fractions = this->_state->mole_fractions_liquid_double();
    return new VectorDoubleContainer(fractions);
}

const VectorDoubleContainer* _AbstractState::mole_fractions_vapor(void) const
{
    std::vector<double> fractions = this->_state->mole_fractions_vapor_double();
    return new VectorDoubleContainer(fractions);
}

const char* _AbstractState::ideal_curve(const char* type,
                                        const double* T,
                                        unsigned long T_length,
                                        const double* p,
                                        unsigned long p_length) const
{
    std::vector<double> T_vec;
    std::vector<double> p_vec;

    T_vec.assign(T, T + T_length);
    p_vec.assign(p, p + p_length);

    const std::string s = type;

    try
    {
        this->_state->ideal_curve(s, T_vec, p_vec);
        return (char*)"";
    }
    catch(const std::exception& e)
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }
}

const char* _AbstractState::viscosity_contributions(double dilute,
                                                    double initial_density,
                                                    double residual,
                                                    double critical) const
{
    try
    {
        this->_state->viscosity_contributions(dilute, initial_density,
            residual, critical);
        return (char*)"";
    }
    catch(const std::exception& e)
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }
}

const char* _AbstractState::conductivity_contributions(double dilute,
                                                       double initial_density,
                                                       double residual,
                                                       double critical) const
{
    try
    {
        this->_state->conductivity_contributions(dilute, initial_density,
            residual, critical);
        return (char*)"";
    }
    catch(const std::exception& e)
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }
}

const char* _AbstractState::conformal_state(const char* reference_fluid, double T, double rhomolar) const
{
    try
    {
        this->_state->conformal_state(reference_fluid, T, rhomolar);
        return (char*)"";
    }
    catch(const std::exception& e)
    {
        std::string s = e.what();
        char *cstr = new char[s.length() + 1];
        strcpy(cstr, s.c_str());
        return cstr;
    }   
}

/// Constructor
_AbstractState* create_instance(const char* backend, const char* fluidName)
{
    try
    {
        return new _AbstractState(backend, fluidName);
    } 
    // Print exception message and tell D to throw
    catch(const std::exception& e) 
    {
        // std::cout << e.what() << std::endl;
        return new _AbstractState(e.what());
    }
}

// Destructor
void delete_instance(_AbstractState *&inst)
{
    delete inst;
    inst = 0;
}
