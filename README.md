# CoolProp D Language Wrapper #

## Building the CoolProp Static Library ##

Navigate the the directory where you would like to save the CoolProp repository
Check out the sources for CoolProp
```shell
git clone https://github.com/CoolProp/CoolProp --recursive
```
Move into the folder you just created
```shell
cd CoolProp
```

(TEMPORARY), add the following lines to CMakeLists.txt

```cmake
option(NO_ERROR_CATCHING
    "Do not catch errors, let them be thrown"
    OFF)
IF (NO_ERROR_CATCHING)
    add_definitions(-DNO_ERROR_CATCHING) 
ENDIF()
```

Make a build folder and build using cmake
```shell
mkdir -p build && cd build
cmake .. -DCOOLPROP_STATIC_LIBRARY=ON -DNO_ERROR_CATCHING=ON
cmake --build .
```

## Installing the D wrapper (Linux) (TEMPORARY) ##

1. Clone the repository 
```shell
git clone https://gitlab.com/treddell/CoolPropD.git
```

2. Move into the created repository and run the installation script
```shell
./install.sh /directory/of/CoolProp/repository
```

Replacing the path with the location of the CoolProp repository.

## Using in a dub project ##
1. Add the repository as a local package using `dub add-local `
2. Add "coolprop" to your projects dependencies field in dub.json
3. Also add `/path/to/CoolPropD/source/cpp_layer.o` and `/path/to/CoolPropD/source.libCoolProp.a` to the "sourceFiles" field in dub.json

## Example usage ##

\# main.d
```d
import std.stdio;
import coolprop;

void main()
{
    // Do some high level interface calls
    writefln("enthalpy = %g (J/kg)", PropsSI("H", "P", 8.0E6, "T", 500, "CO2"));
    writefln("density = %g (kg/m3)", PropsSI("Dmass", "P", 8.0E6, "T", 500, "CO2"));

    // Do some low level interface calculations
    auto fluid = new AbstractState("HEOS", "CO2");
    fluid.update(PT_INPUTS, 8.0E6, 500);
    writefln("density = %g (kg/m3)", fluid.rhomass);
    writefln("cp = %g (J/kg.K)", fluid.cpmass);
    writefln("d(h)/dT|p = %g (J/kg.K)", fluid.first_partial_deriv(iHmass, iT, iP));
    writefln("d^2(h)/dT|p dp|T = %g (m^2/kg)", fluid.second_partial_deriv(iHmass, iT, iP, iP, iT));
}
```